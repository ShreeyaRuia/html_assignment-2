const signupForm = document.querySelector('#registerationForm');
const loginForm = document.querySelector('#loginForm1');
const signOut = document.querySelector('#signOut');
const allUser = document.querySelector('#allUser');
const signUpBtn = document.querySelector('#signUpBtn');
const loginBtn = document.querySelector('#loginBtn');
let flag=1;



companyData = [{
    "firstName": "Marthena",
    "lastName": "Haffner",
    "email": "mhaffner0@amazon.co.jp",
    "gender": "Female",
    "userName": "mhaffner0",
    "password": "uDKE29qOWL",
    "role": "admin"
}, {
    "firstName": "Karney",
    "lastName": "Skipsey",
    "email": "kskipsey1@narod.ru",
    "gender": "Male",
    "userName": "kskipsey1",
    "password": "B7n9dyiqXT",
    "role": "admin"
}, {
    "firstName": "Linda",
    "lastName": "Wolpert",
    "email": "lwolpert2@tripod.com",
    "gender": "Male",
    "userName": "lwolpert2",
    "password": "ZP1G5Y",
    "role": "admin"
}, {
    "firstName": "Marrilee",
    "lastName": "McBrier",
    "email": "mmcbrier3@tumblr.com",
    "gender": "Male",
    "userName": "mmcbrier3",
    "password": "FyLZzo2E0F",
    "role": "admin"
}, {
    "firstName": "Feliks",
    "lastName": "Ratledge",
    "email": "fratledge4@mac.com",
    "gender": "Male",
    "userName": "fratledge4",
    "password": "El44zZmOM",
    "role": "admin"
}, {
    "firstName": "Perle",
    "lastName": "McCowen",
    "email": "pmccowen5@ed.gov",
    "gender": "Male",
    "userName": "pmccowen5",
    "password": "DYgEZsevF",
    "role": "sales"
}, {
    "firstName": "Bertie",
    "lastName": "Hanley",
    "email": "bhanley6@google.co.uk",
    "gender": "Male",
    "userName": "bhanley6",
    "password": "rAxbJbp8",
    "role": "sales"
}, {
    "firstName": "Elise",
    "lastName": "Babidge",
    "email": "ebabidge7@engadget.com",
    "gender": "Female",
    "userName": "ebabidge7",
    "password": "E9phHQ",
    "role": "sales"
}, {
    "firstName": "Aeriela",
    "lastName": "Turvey",
    "email": "aturvey8@arstechnica.com",
    "gender": "Female",
    "userName": "aturvey8",
    "password": "oQzdJFrq",
    "role": "sales"
}, {
    "firstName": "Amandy",
    "lastName": "Klimowski",
    "email": "aklimowski9@yahoo.com",
    "gender": "Male",
    "userName": "aklimowski9",
    "password": "I6T5ZEd",
    "role": "sales"
}, {
    "firstName": "Sayre",
    "lastName": "Curm",
    "email": "scurma@dedecms.com",
    "gender": "Male",
    "userName": "scurma",
    "password": "WPqM0mpxCU",
    "role": "operation"
}, {
    "firstName": "Judie",
    "lastName": "Primarolo",
    "email": "jprimarolob@hud.gov",
    "gender": "Female",
    "userName": "jprimarolob",
    "password": "YfIDUZcgs",
    "role": "operation"
}, {
    "firstName": "Hardy",
    "lastName": "Tosney",
    "email": "htosneyc@archive.org",
    "gender": "Female",
    "userName": "htosneyc",
    "password": "umFgzYMKQ9",
    "role": "operation"
}, {
    "firstName": "Consolata",
    "lastName": "Stailey",
    "email": "cstaileyd@squarespace.com",
    "gender": "Female",
    "userName": "cstaileyd",
    "password": "Leejskpv42o",
    "role": "operation"
}, {
    "firstName": "Rustie",
    "lastName": "Stockney",
    "email": "rstockneye@ucla.edu",
    "gender": "Female",
    "userName": "rstockneye",
    "password": "xNHoq3FmKCP",
    "role": "operation"
}]

signupForm.addEventListener('submit', (e) => {
    e.preventDefault();
    newUser = {
        "userName": document.getElementById('userName').value,
        "email": document.getElementById('signEmail').value,
        "firstName": document.getElementById('firstName').value,
        "lastName": document.getElementById('lastName').value,
        "password": document.getElementById('signPassword').value,
        "gender": signupForm.elements["gender"].value,
        "role": signupForm.elements["role"].value
    }
    companyData.push(newUser);
    signupForm.reset();
    //console.log(companyData);
    document.getElementById('signUpAlert').style.display = "";
});


function addUserDetails(details) {
    text = ""
    for (key in details) {
        if (key !== "password") {
            text += key + " : " + details[key] + "\n"
        }
    }
    sessionStorage.setItem("recentUser", JSON.stringify(details));
    localStorage.setItem("recentUser", JSON.stringify(details));
    document.getElementById('userDetails').value = text;
    addCompanyDetails(details);
    document.getElementById('dashboard').style.display = "";
}

function addCompanyDetails(details) {
    text = ""
    if (details.role === "admin") {
        for (let i = 0; i < companyData.length; i++) {
            text += `-------Employee ${i+2}--------` + "\n"
            employee = companyData[i];
            for (key in employee) {
                if (key !== "password") {
                    text += key + " : " + employee[key] + "\n"
                }
            }

        }
    } else {
        for (let i = 0; i < companyData.length; i++) {
            employee = companyData[i];
            if (employee.role === details.role) {
                text += `-------Employee ${i+2}--------` + "\n"
                for (key in employee) {
                    if (key !== "password") {
                        text += key + " : " + employee[key] + "\n"
                    }
                }

            }
        }
    }
    document.getElementById('employeeDetails').value = text;
}

function loginVerify(){
  currentUser = {
        "email": document.getElementById('loginEmail').value,
        "loginPassword": document.getElementById('loginPassword').value,
    }
    found = companyData.find(obj => obj.email == currentUser.email)
    if (found != undefined) {
        console.log("found user");
        if (found.password == currentUser.loginPassword) {
            document.getElementById('alertMessage').style.display = "none";
            console.log("Success Login");
            document.getElementById("loginBlock").style.display = "none";
            addUserDetails(found);
        } else {
            document.getElementById('loginPassword').value = "";
            console.log("Unsucessful login");
            document.getElementById('alertMessage').style.display = "";
        }
    } else {
        document.getElementById('alertMessage').style.display = "";
        console.log("User not found")
        loginForm.reset();
    }
    loginForm.reset();
}


loginForm.addEventListener('submit', (e) => {
    e.preventDefault();
    loginVerify();
    //console.log(currentUser);
});

signOut.addEventListener('click', (e) => {
    sessionStorage.removeItem('recentUser');
    localStorage.removeItem('recentUser');
    document.getElementById('dashboard').style.display = "none";
    document.getElementById("loginBlock").style.display = "";
});


signUpBtn.addEventListener('click', (e) => {
    document.getElementById("loginBlock").style.display = "none";
     document.getElementById('signupBlock').style.display = "";
});

loginBtn.addEventListener('click', (e) => {
  document.getElementById('signUpAlert').style.display = "none";
    document.getElementById("loginBlock").style.display = "";
     document.getElementById('signupBlock').style.display = "none";
});


allUser.addEventListener('click', (e) => {
let text=""
if(flag==1){
for (let i = 0; i < companyData.length; i++) {
            text += `-------Employee ${i+1}--------` + "\n"
            employee = companyData[i];
            for (key in employee) {
                if (key === "email" || key==="userName") {
                    text += key + " : " + employee[key] + "\n"
                }
            }

        }
        document.getElementById('allEmployee').value=text;
        document.getElementById('employeeBlock').style.display="";
        flag=0;
}
else{
  document.getElementById('employeeBlock').style.display="none";
  flag=1;
}

  });



if (localStorage.getItem('recentUser')!=null){
  sessionStorage.setItem("recentUser",localStorage.getItem('recentUser'));
  currentUser=JSON.parse(sessionStorage.getItem('recentUser'));
  document.getElementById('loginEmail').value=currentUser.email;
  document.getElementById('loginPassword').value=currentUser.password;
  loginVerify();
}